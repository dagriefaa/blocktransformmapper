<div align="center">
<img src="header.png" alt="Block Scaling Tools" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1808581664<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1808581664?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1808581664?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1808581664?color=%231b2838&logo=steam&style=for-the-badge)
</div>

In Besiege, there are two 'hidden' features in the building system that are considered _essential_ to building complex machines - removing the limits on block settings, and changing the size of blocks. You can't create things with them in the vanilla game, you can certainly _load_ them! Many machines (particularly the popular ones) on the Steam Workshop use these features for this reason, to great effect.

So, mods are required to allow people to build machines with them without rifling through the machine savefiles.<br>
NoBounds handles the first feature. <br>
This mod handles the second. Among other things.

## Features
It is _by far_ the most complete and properly-integrated scaling mod to ever be made for Besiege. Nothing - not EasyScale, not HardScale, not PBP - even comes close.
* The Scaling Gizmo allows fast and intuitive handling of multi-block selections
* The Transform Mapper allows precise tuning of individual blocks (with independent copy/paste and draggable infinite slider inputs)
* A numerical display for transform gizmos allows you to know _precisely_ how much you're moving a selection, for fast and repeatable actions
* It fully supports Build Surfaces, making them responsive to scaling and preventing nasty edge cases
* It's integrated so seamlessly into the vanilla toolset that it is indistinguishable from the vanilla game.

It's a self-contained Bracecube management solution:
* Create Bracecubes by scaling a brace to 0 on all axes with the Transform Mapper, and undoing
* Warp Bracecubes by pressing Ctrl+D while dragging on the rotation gizmo

It unhides vanilla-compatible hidden options:
* Build Surface (visual) thickness slider
* Scaling/Smooth Surface Blocks

It handles a laundry list of bugs and tweaks:
* Increases the maximum amount of keys on a keybinding from three to five
* Decreases the minimal size of surface nodes, making them easier to get around
* Ensures rotation arrows (on wheels and hinges) maintain uniform scale on scaled blocks, and hides them when the HUD is hidden
* Enables placing intersecting blocks by default
* Brings the Level Editor transform tools to parity with the Advanced Building transform tools:
    * The translate/move tool snaps to the local orientation of the entity instead of locking it to a global grid.
    * The snap increments/grid size fields can be scrolled up and down, like the block snap fields.
    * The transform tools hide when holding LShift or when drag-selecting.

And the best part is that it's fully multiplayer compatible.

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
For the most part, if you can't figure out how the Level Editor equivalents work you probably aren't ready to use this mod.

Additional usage instructions (such as **copy/paste hotkeys**) can be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1808581664).

## Contributors
- [Lassebq](https://gitlab.com/Lassebq)

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
