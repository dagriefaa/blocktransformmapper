﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockScalingTools {
    class ScrollingValue : MonoBehaviour {


        public bool centering = true;
        public float step = 0.1f;
        public float[] keptSteps = new float[0];
        protected bool mouseOver;
        Selectors.ValueHolderCentering hodl;

        void Start() {
            hodl = GetComponent<Selectors.ValueHolderCentering>();
        }

        protected void Update() {
            if (mouseOver) {
                if (InputManager.ScrollFieldValue() > 0f) {
                    float max = float.PositiveInfinity;
                    for (int i = 0; i < keptSteps.Length; i++) {
                        if (keptSteps[i] > hodl.Value()) {
                            max = keptSteps[i];
                            break;
                        }
                    }
                    float value = hodl.Value() + step;
                    if (max < value) {
                        value = max;
                    }
                    hodl.SetValue(value);
                }
                else if (InputManager.ScrollFieldValue() < 0f) {
                    float min = 0f;
                    for (int j = keptSteps.Length - 1; j >= 0; j--) {
                        if (keptSteps[j] < hodl.Value()) {
                            min = keptSteps[j];
                            break;
                        }
                    }
                    float value = hodl.Value() - step;
                    if (min > value) {
                        value = min;
                    }
                    hodl.SetValue(value);
                }
            }
        }

        protected virtual void OnMouseEnter() {
            mouseOver = true;
            StatMaster.DisableCameraZoom(true);
        }

        protected virtual void OnMouseExit() {
            mouseOver = false;
            StatMaster.DisableCameraZoom(false);
        }

        protected void OnDisable() {
            if (mouseOver) {
                StatMaster.DisableCameraZoom(false);
            }
        }
    }
}

