using System.Collections;
using System.Linq;
using UnityEngine;

public class OutlineFillDisabler : MonoBehaviour {
    IEnumerator Start() {
        yield return new WaitUntil(() => PrefabMaster.LevelPrefabs?.Count > 0);
        var primitives = PrefabMaster.LevelPrefabs[(int)StatMaster.Category.Primitives].Values.ToList();
        primitives.Add(PrefabMaster.GetPrefab(StatMaster.Category.EnvironmentFoliage, 2037)); // small valley
        primitives.Add(PrefabMaster.GetPrefab(StatMaster.Category.EnvironmentFoliage, 2024)); // low hill
        primitives.Add(PrefabMaster.GetPrefab(StatMaster.Category.EnvironmentFoliage, 2078)); // dune
        foreach (var prefab in primitives) {
            prefab?.GetComponentsInChildren<cakeslice.Outline>(true).ToList().ForEach(x => x.useFill = false);
        }
        Destroy(this);
    }
}