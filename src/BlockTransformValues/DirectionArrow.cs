﻿using UnityEngine;

namespace BlockScalingTools {

    /// <summary>
    /// Scaling support for motor block direction arrows.
    /// </summary>
    public class DirectionArrow : MonoBehaviour {

        SteeringWheel hinge;
        CogMotorControllerHinge motor;
        Renderer renderer;

        void Start() {
            renderer = GetComponent<Renderer>();
            hinge = GetComponentInParent<SteeringWheel>();
            motor = GetComponentInParent<CogMotorControllerHinge>();
        }

        void Update() {
            if (!renderer || !(hinge || motor)) {
                DestroyImmediate(this);
                return;
            }

            if (renderer) {
                renderer.enabled = !StatMaster.hudHidden && !ReferenceMaster.activeMachineSimulating;
            }

            if (transform.parent.lossyScale != Vector3.zero) {
                int flipped = (hinge ? hinge.Flipped : motor.Flipped) ? -1 : 1;

                if (hinge) {
                    if (hinge.BlockID == (int)BlockType.SteeringHinge) {
                        transform.localScale = new Vector3(
                            transform.localScale.x == 0 ? 0f : 1 / transform.parent.localScale.z,
                            transform.localScale.y == 0 ? 0f : flipped * 1 / transform.parent.localScale.x,
                            1f / transform.parent.localScale.y);
                        transform.localScale *= Mathf.Max(transform.parent.localScale.x, transform.parent.localScale.z);
                    }
                    else {
                        transform.localScale = new Vector3(
                            transform.localScale.x == 0 ? 0f : 1 / transform.parent.localScale.x,
                            transform.localScale.y == 0 ? 0f : flipped * 1 / transform.parent.localScale.y,
                            1f / transform.parent.localScale.z);
                        transform.localScale *= Mathf.Max(transform.parent.localScale.x, transform.parent.localScale.y);
                    }
                }
                else {
                    int isDrill = (motor.BlockID == (int)BlockType.Drill) ? -1 : 1;
                    int isCog = (motor.BlockID == (int)BlockType.CogMediumPowered) ? -1 : 1;
                    transform.localScale = new Vector3(
                        transform.localScale.x == 0 ? 0f : flipped * 1 / transform.parent.localScale.x,
                        transform.localScale.y == 0 ? 0f : isCog / transform.parent.localScale.y,
                        isDrill / transform.parent.localScale.z);
                    transform.localScale *= Mathf.Max(transform.parent.localScale.x, transform.parent.localScale.y);
                }
            }
        }
    }
}