﻿using UnityEngine;
using System;
using System.Collections;

namespace BlockScalingTools {
    public class HorizontalSlideAnimation : MonoBehaviour {

        public float openPosition;
        public float closedPosition;
        public float animationTime = 0.5f;
        public Transform hoverArea;
        public Transform mover;

        public Func<bool> ExternalControl = null;

        public bool Extended { get; private set; } = false;

        Renderer indicator;
        Camera hudCamera;
        bool isOpen;
        bool animating;
        Vector3 iconRotation;

        public static HorizontalSlideAnimation Create(GameObject selfObject, float openPosition, float closedPosition, Func<bool> externalControl,
            Transform mover = null,
            float animationTime = 0.5f) {
            HorizontalSlideAnimation self = selfObject.AddComponent<HorizontalSlideAnimation>();
            self.openPosition = openPosition;
            self.closedPosition = closedPosition;
            self.mover = (mover ?? self.transform);
            self.animationTime = animationTime;
            self.ExternalControl = externalControl;
            return self;
        }

        public static HorizontalSlideAnimation Create(GameObject selfObject, float openPosition, float closedPosition, Transform hitbox,
            Transform mover = null, float animationTime = 0.5f) {
            HorizontalSlideAnimation self = selfObject.AddComponent<HorizontalSlideAnimation>();
            self.openPosition = openPosition;
            self.closedPosition = closedPosition;
            self.mover = (mover ?? self.transform);
            self.animationTime = animationTime;
            self.hoverArea = hitbox;
            return self;
        }

        private void Start() {
            hudCamera = GameObject.FindGameObjectWithTag("hudCamera").GetComponent<Camera>();
            indicator = GetComponent<Renderer>();
            if (indicator) iconRotation = indicator.transform.localEulerAngles;
        }

        private void LateUpdate() {
            Extended = (ExternalControl == null) ? MouseOver() : ExternalControl();
            if (Extended == isOpen) {
                return;
            }
            base.StopAllCoroutines();
            base.StartCoroutine(IEAnimate(Extended, 0.2f, animationTime));
            if (indicator) indicator.transform.localEulerAngles = isOpen ? (iconRotation + new Vector3(0, 0, 180)) : iconRotation;
        }

        private bool MouseOver() {
            Vector3 point = hudCamera.ScreenToWorldPoint(Input.mousePosition);
            Bounds bounds = new Bounds(hoverArea.position, hoverArea.lossyScale);
            return bounds.Contains(new Vector3(point.x, point.y, hoverArea.position.z));
        }

        private IEnumerator IEAnimate(bool open, float wait, float duration) {
            isOpen = open;
            if (!animating) {
                yield return new WaitForSecondsRealtime(wait);
            }
            animating = true;
            float target = (open) ? openPosition : closedPosition;
            for (float t = 0f; t < duration; t += Time.deltaTime) {
                mover.localPosition = new Vector3(Mathf.Lerp(mover.localPosition.x, target, t / duration), mover.localPosition.y, mover.localPosition.z);
                yield return null;
            }
            mover.localPosition = new Vector3(target, mover.localPosition.y, mover.localPosition.z);
            animating = false;
            yield break;
        }
    }

}
