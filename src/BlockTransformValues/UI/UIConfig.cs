﻿using System.Collections;
using UnityEngine;

namespace BlockScalingTools {

    /// <summary>
    /// Specifies default materials and values for this mod.
    /// </summary>
    public class UIConfig : MonoBehaviour {

        public static Material BACKGROUND_MAT = UIFactory.BLACK_MAT;
        public static Material BACKGROUND_MASK_MAT = UIFactory.BLACK_MAT_MASKED;

        public static Material BUTTON_ACTIVE_MAT = UIFactory.CLEAR_MAT;
        public static Material BUTTON_INACTIVE_MAT = UIFactory.CLEAR_MAT;

        public static Material CURSOR_MAT = UIFactory.WHITE_MAT;

        public static Material ICON_MAT_STENCIL = new Material(Shader.Find("Custom/Stencil/Alpha Blended"));
        public static Material TEXT_MAT_STENCIL = null; // setup in UIFactory.OnSceneChanged

        static UIConfig() {
            ICON_MAT_STENCIL.SetColor("_TintColor", Color.white);
            ICON_MAT_STENCIL.SetFloat("_StencilVal", 1);
        }

    }
}