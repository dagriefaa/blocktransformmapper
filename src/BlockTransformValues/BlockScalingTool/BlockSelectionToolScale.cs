﻿using Modding;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    public class BlockSelectionToolScale : BlockSelectionTool {

        public static BlockSelectionToolScale Instance { get; private set; }

        public static void Consume(BlockSelectionTool bst) {
            BlockSelectionToolScale consumer = bst.gameObject.AddComponent<BlockSelectionToolScale>();
            AddPiece.Instance.selectionController = consumer;
            AdvancedBlockEditor.Instance.selectionController = consumer;
            consumer.selectionReference = bst.selectionReference;
            consumer.lastClickedTransformInfo = bst.lastClickedTransformInfo;
            bst.CopyProperties(consumer);
            consumer.ToolTransform = BlockScaleToolController.HandleInstance.transform;
            consumer.Init(AdvancedBlockEditor.Instance);
            Destroy(bst);

            Instance = consumer;
        }

        /// <summary>
        /// Make the drag selection update now instead of the next time the user moves their mouse.
        /// </summary>
        public void ForceUpdateDragSelection() {
            lastPos = -Vector2.one;
        }

        protected override Dictionary<long, ISelectable> GetSelectedObjects(Vector3 startPos, Vector3 endPos) {
            var blocks = base.GetSelectedObjects(startPos, endPos);

            // if surface-only selection is active, filter out non-node blocks
            if (SurfaceSelector.IsFiltering) {
                return blocks
                    .Where(x =>
                        (x.Value as BlockBehaviour).Prefab.Type == BlockType.BuildNode
                        || (x.Value as BlockBehaviour).Prefab.Type == BlockType.BuildEdge
                    ).ToDictionary(x => x.Key, x => x.Value);
            }

            return blocks;
        }

        public override bool CanSelect() {
            Machine machine = Machine.Active();
            if (machine != null && StatMaster.advancedBuilding && !machine.isSimulating && !StatMaster.Mode.selectSymmetryPivot) {
                switch (StatMaster.Mode.selectedTool) {
                    case StatMaster.Tool.Translate:
                    case StatMaster.Tool.Rotate:
                    case StatMaster.Tool.Mirror:
                    case StatMaster.Tool.Scale:
                        return true;
                }
            }
            return false;
        }

    }
}