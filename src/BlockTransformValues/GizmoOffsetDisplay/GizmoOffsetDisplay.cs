﻿using Modding;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    public class GizmoOffsetDisplay : MonoBehaviour {

        Transform TranslateTool { get { return Mod.EntityTransformToolActive() ? entityTranslateTool : blockTranslateTool; } }
        Transform RotateTool { get { return Mod.EntityTransformToolActive() ? entityRotateTool : blockRotateTool; } }
        Transform ScaleParent { get { return Mod.EntityTransformToolActive() ? entityScaleTool : BlockScaleTool.ScaleParent; } }

        StatMaster.Tool SelectedTool { get { return Mod.EntityTransformToolActive() ? StatMaster.Mode.LevelEditor.selectedTool : StatMaster.Mode.selectedTool; } }

        bool initialised = false;
        bool _value;
        string ID = "GizmoOffsetDisplay";
        bool DefaultValue = true;
        public bool Enabled {
            get {
                if (!initialised) {
                    Enabled = Configuration.GetData().HasKey(ID) ? Configuration.GetData().ReadBool(ID) : DefaultValue;
                    initialised = true;
                }
                return _value;
            }
            set {
                if (value.Equals(_value)) {
                    return;
                }
                _value = value;
                Configuration.GetData().Write(ID, value);
            }
        }

        DynamicText blockText;
        DynamicText offsetText;
        GameObject blockDisplay;
        GameObject offsetDisplay;

        Transform blockTranslateTool;
        Transform entityTranslateTool;
        Transform blockRotateTool;
        Transform entityRotateTool;
        Transform entityScaleTool;

        SpriteRenderer toggle;

        Vector3 lastPosition = Vector3.zero;
        Vector3 lastRotation = Vector3.zero;

        static Sprite toggleSprite = null;
        static readonly Color CYAN = new Color(0.012f, 1, 0.847f, 0.5f);

        void Start() {
            this.transform.SetParent(GameObject.Find("HUD").transform.FindChild("BottomBar"), false);
            this.transform.localPosition = new Vector3(0, -1, 0);
            this.gameObject.layer = UIFactory.HUD_LAYER;

            blockDisplay = UIFactory.Empty(this.transform, Vector3.zero, "BlockDisplay").gameObject;
            UIFactory.Background(blockDisplay.transform, Vector3.zero, new Vector3(3.5f, 0.4f, 1), true);
            UIFactory.Background(blockDisplay.transform, Vector3.zero, new Vector3(3.5f, 0.4f, 1), true);
            blockText = UIFactory.Text(blockDisplay.transform, Vector3.zero, "(0.000m, 0.000m, 0.000m)", 0.25f, maxCharWidth: 26);

            offsetDisplay = UIFactory.Empty(this.transform, new Vector3(0, -0.4f, 0), "OffsetDisplay").gameObject;
            UIFactory.Background(offsetDisplay.transform, Vector3.zero, new Vector3(3.5f, 0.4f, 1), true);
            UIFactory.Background(offsetDisplay.transform, Vector3.zero, new Vector3(3.5f, 0.4f, 1), true);
            offsetText = UIFactory.Text(offsetDisplay.transform, Vector3.zero, "(00,0,00)", 0.25f, maxCharWidth: 26);

            toggleSprite = toggleSprite
                ?? GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Top Right Adjecent)/Buttons/INFO/Icon").GetComponent<SpriteRenderer>().sprite;

            Transform abButton = GameObject.Find("HUD").transform.FindChild("TopBar/Align (Top Right)/SettingsContainer/Settings (Fold Out)/GENERAL/BuildingTools");
            toggle = UIFactory.Sprite(null, toggleSprite, scale: Vector3.one * 0.05f, color: CYAN);
            Transform toggleButton = ToggleButton.Create(abButton, toggle.transform,
                () => Enabled,
                x => {
                    Enabled = x;
                    toggle.color = x ? CYAN : new Color(1, 1, 1, 0.25f);
                },
                new Vector3(0.22f, -0.22f, 0),
                Vector3.one * 0.3f,
                name: "GizmoOffsetDisplayToggle").transform;
            Tooltip.Create(toggleButton, Tooltip.Direction.Left, "GIZMO OFFSET DISPLAY");
        }

        void LateUpdate() {
            if (Mod.SceneNotPlayable())
                return;
            if (!Machine.Active())
                return;

            this.transform.localPosition = new Vector3(0, Mathf.Lerp(-3.5f, -1, (OptionsMaster.BesiegeConfig.UIScale - 50) / 50));

            if (Mod.BlockTransformToolActive() && !blockTranslateTool) {
                blockTranslateTool = Resources.FindObjectsOfTypeAll<BlockTranslateTool>().First().Gizmo();
                blockRotateTool = Resources.FindObjectsOfTypeAll<BlockRotateTool>().First().Gizmo();
            }
            if (Mod.EntityTransformToolActive() && !entityTranslateTool && StatMaster.isMP) {
                entityTranslateTool = Resources.FindObjectsOfTypeAll<EntityTranslateTool>().First().Gizmo();
                entityRotateTool = Resources.FindObjectsOfTypeAll<EntityRotateTool>().First().Gizmo();
                entityScaleTool = Resources.FindObjectsOfTypeAll<EntityScaleTool>().First().scaleParent;
            }
            if (Enabled && (Mod.BlockTransformToolActive() || Mod.EntityTransformToolActive())) {
                bool somethingSelected = Mod.EntityTransformToolActive() || AdvancedBlockEditor.Instance.selectionController.Selection.Count > 0;
                Transform targetTransform;
                if (Mod.EntityTransformToolActive()) {
                    targetTransform = LevelEditor.Instance.selectionController.Selection.Last().GetTransform();
                }
                else if (AdvancedBlockEditor.Instance.selectionController.Selection.Count > 0) {
                    targetTransform = AdvancedBlockEditor.Instance.selectionController.Selection.Last().GetTransform();
                }
                else {
                    targetTransform = Machine.Active().BuildingMachine;
                }

                char suffix;
                Vector3 objectValues;
                switch (SelectedTool) {
                    case StatMaster.Tool.Translate:
                    case StatMaster.Tool.Mirror:
                        suffix = 'm';
                        objectValues = targetTransform.localPosition;
                        break;
                    case StatMaster.Tool.Rotate:
                        objectValues = targetTransform.localEulerAngles;
                        suffix = '°';
                        break;
                    case StatMaster.Tool.Scale:
                        suffix = 'x';
                        objectValues = targetTransform.localScale;
                        break;
                    default:
                        throw new System.Exception("[BlockScalingTools] Invalid transform tool in gizmo value display");
                }

                bool scalingMachine = StatMaster.Mode.selectedTool == StatMaster.Tool.Scale && !somethingSelected;
                blockDisplay.SetActive(true);
                blockText.SetText(scalingMachine
                    ? "(-.---x, -.---x, -.---x)"
                    : $"({objectValues.x:0.000}{suffix}, {objectValues.y:0.000}{suffix}, {objectValues.z:0.000}{suffix})");

                if (!(StatMaster.Mode.isTranslating || StatMaster.Mode.isRotating || StatMaster.Mode.isScaling)) {
                    lastPosition = TranslateTool.localPosition;
                    lastRotation = RotateTool.localEulerAngles;
                    offsetDisplay.SetActive(false);
                }
                else {
                    Vector3 toolDelta;
                    float snap;
                    switch (SelectedTool) {
                        case StatMaster.Tool.Translate:
                        case StatMaster.Tool.Mirror:
                            toolDelta = TranslateTool.InverseTransformDirection(TranslateTool.localPosition - lastPosition);
                            snap = Mod.EntityTransformToolActive() ? EntityTranslateTool.SNAP_VALUE : BlockTranslateTool.SNAP_VALUE;
                            break;
                        case StatMaster.Tool.Rotate:
                            toolDelta = TransformSelector.BoundRotation(RotateTool.localEulerAngles - lastRotation);
                            snap = Mod.EntityTransformToolActive() ? EntityRotateTool.SNAP_VALUE : BlockRotateTool.SNAP_VALUE;
                            break;
                        case StatMaster.Tool.Scale:
                            toolDelta = ScaleParent.localScale;
                            snap = Mod.EntityTransformToolActive() ? EntityScaleTool.SNAP_VALUE : BlockScaleTool.SnapValue;
                            break;
                        default:
                            throw new System.Exception("[Block Scaling Tools] Invalid transform tool in gizmo value display");
                    }
                    toolDelta = Snap(toolDelta, snap);

                    if (SelectedTool == StatMaster.Tool.Scale) {
                        offsetDisplay.SetActive(toolDelta != Vector3.one);
                        offsetText.SetText(
                        $"({toolDelta.x:0.000}{suffix}, {toolDelta.y:0.000}{suffix}, {toolDelta.z:0.000}{suffix})");
                    }
                    else {
                        offsetDisplay.SetActive(toolDelta != Vector3.zero);
                        offsetText.SetText(
                        $"({toolDelta.x:+0.000;-0.000;0.000}{suffix}, " +
                        $"{toolDelta.y:+0.000;-0.000;0.000}{suffix}, " +
                        $"{toolDelta.z:+0.000;-0.000;0.000}{suffix})");
                    }
                }
            }
            else {
                blockDisplay.SetActive(false);
                offsetDisplay.SetActive(false);
            }
        }

        static Vector3 Snap(Vector3 value, float snap) {
            return !InputManager.AdvancedBuilding.LeftCtrlKey() ? TransformTool.Snap(value, snap) : value;
        }

    }
}