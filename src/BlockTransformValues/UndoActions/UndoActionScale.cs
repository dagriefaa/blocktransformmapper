﻿using System;
using UnityEngine;
using Modding;
using Modding.Blocks;

/**
 * Scaling undo actions aren't shared over the network either.
 */
namespace BlockScalingTools {

    // Token: 0x020007CE RID: 1998
    public class UndoActionScale : UndoAction {

        protected Vector3 lastScale;
        protected Vector3 scale;
        protected Vector3 lastPosition;
        protected Vector3 position;
        protected bool isGizmoAction;

        public UndoActionScale(Machine m, Guid blockGuid, Vector3 scale, Vector3 lastScale) {
            this.scale = scale;
            this.lastScale = lastScale;
            this.guid = blockGuid;
            this.changesTransform = true;
            this.machine = m;
            this.isGizmoAction = false;
        }

        public UndoActionScale(Machine m, Guid blockGuid, Vector3 scale, Vector3 lastScale, Vector3 position, Vector3 lastPosition) {
            this.scale = scale;
            this.lastScale = lastScale;
            this.guid = blockGuid;
            this.position = position;
            this.lastPosition = lastPosition;
            this.changesTransform = true;
            this.machine = m;
            this.isGizmoAction = true;
        }


        public override bool Redo() {
            this.machine.GetBlock(guid, out BlockBehaviour block);
            if (isGizmoAction) {
                BlockScaleTool.ScaleBlock(block, scale);
            }
            else {
                TransformSelector.ScaleBlock(block, scale);
            }
            ModNetworking.SendToAll(
                Messages.Scale.CreateMessage(
                    new object[] { Block.From(guid), this.scale, isGizmoAction }));
            if (isGizmoAction) {
                this.machine.MoveBlock(this.guid, this.machine.BuildingMachine.TransformPoint(position));
            }

            return true;
        }

        public override bool Undo() {
            this.machine.GetBlock(guid, out BlockBehaviour block);
            if (isGizmoAction) {
                BlockScaleTool.ScaleBlock(block, lastScale);
            }
            else {
                TransformSelector.ScaleBlock(block, lastScale);
            }
            ModNetworking.SendToAll(
                Messages.Scale.CreateMessage(
                    new object[] { Block.From(guid), this.lastScale, isGizmoAction }));
            if (isGizmoAction) {
                this.machine.MoveBlock(this.guid, this.machine.BuildingMachine.TransformPoint(lastPosition));
            }

            return true;
        }
    }
}

